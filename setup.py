from distutils.core import setup


setup(name='TUtils',
      version='0.1',
      description='Utilities for genomics, bioinformatics, and HPC computing',
      author='Tito Candelli',
      author_email='t.candelli@gmail.com',
      py_modules=['TUtils'],
      scripts=['scripts/subarray']
    )
